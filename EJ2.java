/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ej2;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class EJ2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       try{
         SAXParserFactory f = SAXParserFactory.newInstance();
         SAXParser s = f.newSAXParser();
         DefaultHandler h = new DefaultHandler() {
               
                boolean bN = false;
                boolean bA = false;
                boolean bV = false;
                boolean bE = false;
                boolean bO = false;
         public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                   
                    if (qName.equalsIgnoreCase("apodo")) {
                        bA = true;
                    }
                    if (qName.equalsIgnoreCase("nombre")) {
                        bN = true;
                    }
                    if (qName.equalsIgnoreCase("videojuego")) {
                        bV = true;
                    }
                    if (qName.equalsIgnoreCase("edad")) {
                        bE = true;
                    }
                    if (qName.equalsIgnoreCase("origen")) {
                        bO = true;
                    }
                  }  
         
          public void characters(char ch[], int start, int length) throws SAXException {
                      if (bA) {
                        System.out.println("Apodo: " + new String(ch, start, length));
                        bA = false;
                    }
                      
                    if (bN) {
                        System.out.println("Nombre: " + new String(ch, start, length));
                        bN = false;
                    }
                    
                    if (bV) {
                        System.out.println("Videojuego: " + new String(ch, start, length));
                        bV = false;
                    }
                    if (bE) {
                        System.out.println("Edad: " + new String(ch, start, length));
                        bE = false;
                    }
                    if (bO) {
                        System.out.println("Origen: " + new String(ch, start, length));
                        System.out.println("------------------------------------------------------------------------------");
                        bO = false;
                    }
                }
         };
         File file = new File("n1.xml");
         InputStream iS= new FileInputStream(file);
         Reader reader = new InputStreamReader(iS, "UTF-8");
         
         InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            s.parse(is, h);
     }catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
}
