/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJ1;

import java.io.File;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class EJ1 {

   
    public static void main(String[] args) {
       DocumentBuilderFactory  factory =  DocumentBuilderFactory.newInstance();
        Scanner scaner = new Scanner (System.in) ;
        String fichero ="n1.xml";
        try{
        DocumentBuilder builder = factory.newDocumentBuilder(); 
	Document document = builder.parse(new File(fichero)); 
	document.getDocumentElement().normalize();
        System.out.println("Elemento raíz: " + document.getDocumentElement() .getNodeName());
        NodeList listadejuegos = document.getElementsByTagName("juego");
       
        for(int i = 0; i < listadejuegos.getLength();i++){
         Node juego = listadejuegos.item(i);
            if(juego.getNodeType() == Node.ELEMENT_NODE){
               Element elemento = (Element) juego ;
               NodeList nodo1 = elemento.getElementsByTagName("titulo").item(0).getChildNodes();
               Node valornodo1 = (Node) nodo1.item(0);
                System.out.println("--> titulo : " + valornodo1.getNodeValue());
                NodeList nodo2 = elemento.getElementsByTagName("genero").item(0).getChildNodes();
                Node valornodo2 = (Node) nodo2.item(0);
                System.out.println("--> genero : " + valornodo2.getNodeValue());
                NodeList nodo3 = elemento.getElementsByTagName("plataforma").item(0).getChildNodes();
                Node valornodo3 = (Node) nodo3.item(0);
                System.out.println("--> Plataforma : " + valornodo3.getNodeValue());
                NodeList nodo4 = elemento.getElementsByTagName("fechadelanzamiento").item(0).getChildNodes();
                Node valornodo4 = (Node) nodo4.item(0);
                System.out.println("--> Fecha de lanzamiento : " + valornodo4.getNodeValue());
            }   System.out.println("----------------------------------");
                
        }
       System.out.println("--> Desea añadir (a) o borrar (b) informcion ");
             if(scaner.nextLine().equals("a")){
                 
                  System.out.println("-> Datos del nuevo Juego:");
			String titulo, genero, plataforma , fechadelanzamiento;
			System.out.println("Inserta un titulo");
			titulo = scaner.nextLine();
			System.out.println("Inserta genero");
			genero = scaner.nextLine();
			System.out.println("Inserta  plataforma de salida");
			plataforma = scaner.nextLine();
                        System.out.println("Inserta fecha de lanzamiento");
			fechadelanzamiento = scaner.nextLine();
			
			Element raiz = document.createElement("juego"); 
			document.getDocumentElement().appendChild(raiz); 
			
			Element elem = document.createElement("titulo"); 
			Text text = document.createTextNode(titulo); 
			raiz.appendChild(elem); 
			elem.appendChild(text); 
			
			Element elem2 = document.createElement("genero"); 
			Text text2 = document.createTextNode(genero);
			raiz.appendChild(elem2); 
			elem2.appendChild(text2); 
			
			Element elem3 = document.createElement("plataforma");
			Text text3 = document.createTextNode(plataforma); 
			raiz.appendChild(elem3);  
			elem3.appendChild(text3);
                        Element elem4 = document.createElement("fechadelanzamiento");
			Text text4 = document.createTextNode(fechadelanzamiento); 
			raiz.appendChild(elem4);  
			elem3.appendChild(text4);
			
			Source source = new DOMSource(document); 
			Result result = new StreamResult(new java.io.File("n1.xml")); 
			Transformer transformer = TransformerFactory.newInstance() .newTransformer(); 
			transformer.transform(source, result);
             }else if(scaner.nextLine().equals("b")){
                System.out.println("-> Inserta el titulo del juego que quieres borrar.");
			String borrar = scaner.nextLine();
			
			NodeList juego2 = document.getElementsByTagName("juego");
			for (int i = 0; i < juego2.getLength(); i ++) { 
				Node juego = juego2.item(i);  
				if (juego.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) juego; 
					
					NodeList nodo= elemento.getElementsByTagName("titulo").item(0).getChildNodes(); 
					Node valornodo = (Node) nodo.item(0); 
					if(valornodo.getNodeValue().equals(borrar)) {
						juego.getParentNode().removeChild(juego);
					}
				}
			}
			
			Source source = new DOMSource(document); 
			Result result = new StreamResult(new java.io.File("n1.xml")); 
			Transformer transformer = TransformerFactory.newInstance() .newTransformer(); 
			transformer.transform(source, result);
			
       }else{
           System.out.println("Escriba a para añadir o b para borrar ");     
                }
        
        }catch(Exception e ){
            e.printStackTrace();
        }
    }
    
}

        